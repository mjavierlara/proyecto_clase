/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 *@author javier Lara
 * @version 20200501
 * @correccion de movimiento de la nave
 */
public class Nave {
     private int posX;
    private int posY;
    private int desX;
    private int desY;
    private final int ANCHO = 10;
    private final int ALTO = 10;
    private Juego juego;
    private Image imagen;
    public Nave(Juego juego){
        this.juego = juego;
        this.posX = 500;
        this.posY = 500;
    }
 /*@ param corregimos el movimiento de la nave*/
    
    void mover() {
        if(posX + desX > juego.getWidth() - this.ANCHO ){
            desX = 0;
        }
        if(posX + desX < 0){
            desX = 0;
        }
        if(posY + desY < 0){
            desY = 0;
        }
        if(posY + desY > juego.getHeight() - this.ALTO){
            desY = 0;
        }
        if(choque()){
            juego.gameOver();
        }
        posX = posX + desX;
        posY = posY + desY;
    }
    
    public void paint(Graphics g) throws IOException{
        File miimagen= new File("src/recursos/player.png");
        imagen=ImageIO.read(miimagen);
        g.drawImage(imagen,posX, posY, null);
        
    }

    void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            desX = -1;
            desY=0;
        }
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            desX = 1;
            desY=0;
        } 
        if(e.getKeyCode() == KeyEvent.VK_UP){
            desY = -1;
            desX=0;
        }
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            desY = 1;
            desX=0;
        }
        
    }
    
    public Rectangle getBounds(){
        return new Rectangle(posX, posY, ANCHO, ALTO);
    }

    private boolean choque() {
        for(Asteroide asteroide:juego.getAsteroides()){
            if(asteroide.getBounds().intersects(this.getBounds())){
                return true;
            }
        }
        for(Marciano marciano:juego.getMarcianos()){
            if(marciano.getBounds().intersects(this.getBounds())){
                return true;
            }
        }
        return false;
    }
}
