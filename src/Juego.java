/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import static com.sun.java.accessibility.util.AWTEventMonitor.addKeyListener;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import static java.awt.image.ImageObserver.ABORT;
import java.util.ArrayList;

import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/**
 *
<<<<<<< HEAD
 * @author javi
=======
 * @author jesus bla bla
>>>>>>> branch 'master' of https://mjavierlara@bitbucket.org/Grvsio22/doged-space.git
 */
public class Juego extends JPanel{
 public ArrayList<Asteroide> getAsteroides() {
        return asteroides;
    }
  public ArrayList<Marciano> getMarcianos() {
        return marcianos;
    }
    
    private Nave nave;
    private ArrayList<Asteroide> asteroides;
    private ArrayList<Marciano> marcianos;
    private Sonido fondo;
    public Juego(){
        // Inicializar los objetos del juego
        nave = new Nave(this);
        asteroides = new ArrayList();
        marcianos = new ArrayList();
        // Añado un asteroide
        anadeAsteroide();
        //Añado marciano
        anadeMarciano();
        //Añado un keyListener
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                nave.keyPressed(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        setFocusable(true);
        Sonido.FONDO.loop();

    }
    
    public static void main(String[] args) throws InterruptedException {
        Juego juego = new Juego();
        JFrame frame = new JFrame("Dodge Space: Asteroides y marcianos");
        frame.add(juego);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1200, 700);
        int iteraciones = 0;
        while(true){
            juego.mover();
            juego.repaint();
            Thread.sleep(10);
            iteraciones++;
            if(iteraciones % 500 == 0){
                juego.anadeAsteroide();
                juego.anadeMarciano();
            }
        }
    }
    
    public void paint(Graphics g){
        super.paint(g);//Necesario para borrar la pantalla antes de volver a pintar
        for(Asteroide asteroide:getAsteroides()){
            asteroide.paint(g);
        }
        for(Marciano marciano:getMarcianos()){
            marciano.paint(g);
        }
        try {
			nave.paint(g);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
   
    /**
     * Se encargará de mover los elementos del juego
     */
    private void mover(){
        nave.mover();
               for(Asteroide asteroide:getAsteroides()){
            asteroide.mover();
        }
        for(Marciano marciano:getMarcianos()){
            marciano.mover();
        }
    }

    private void anadeAsteroide() {
        Asteroide asteroide = new Asteroide(this);
        getAsteroides().add(asteroide);
    }
      private void anadeMarciano() {
        Marciano marciano = new Marciano(this);
        getMarcianos().add(marciano);
    }


    void gameOver() {
        JOptionPane.showMessageDialog(this, "Game over", "Game over", JOptionPane.YES_NO_OPTION);
        System.exit(ABORT);
    }
}