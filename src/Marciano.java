/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
/**
 *
 * @author mariaalonso
 */
public class Marciano extends JPanel{
    private int posX;
    private int posY;
    private int desX = 1;
    private int desY = 1;
    private final int ANCHO = 20;
    private final int ALTO = 20;
    private Juego juego;
    private Image imagen;
    public Marciano(Juego juego){
        this.juego = juego;
        this.posX = (int) (Math.random() * 390);
        this.posY = (int) (Math.random() * 390);
    }

    void mover() {
        if(posX + desX > juego.getWidth() - this.ANCHO ){
            desX = (int) (Math.random() * -1 + -20);
        }
        if(posX + desX < 0){
            desX = 2;
        }
        if(posY + desY < 0){
            desY = 2;
        }
        if(posY + desY > juego.getHeight() - this.ALTO){
            desY = (int) (Math.random() * -1 + -30);
        }
        
        posX = posX + desX;
        posY = posY + desY;
    }
    
    public Rectangle getBounds(){
        return new Rectangle(posX, posY, ANCHO, ALTO);
    }
    
    public void paint(Graphics g){
        File miimagen= new File("src/recursos/Marciano.png");
        try {
			imagen=ImageIO.read(miimagen);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        g.drawImage(imagen,posX, posY, null);
        
    }

}
